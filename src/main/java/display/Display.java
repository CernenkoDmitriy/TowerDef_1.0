package display;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Arrays;

public class Display {

    public static boolean created = false;

    public static JFrame window; // создаю рамку
    public static Dimension size; // создаю размер
    public static Canvas content; // создаю содержимое

    private static BufferedImage buffer;
    private static int[] bufferData;
    private static Graphics bufferGraphics;
    private static int clearColor;

    private static BufferStrategy bufferStrategy;

    public static void create(int width, int height, String title, int _clearColor, int numBuffers) {

        if (created)
            return;

        window = new JFrame(title); // создаю рамку
        size = new Dimension(width, height); // задаю размер

        content = new Canvas(); // создаю содержимое
        content.setPreferredSize(size); // добавляю размер в контент
       // content.setBackground(Color.GRAY); // задаю содержимому цвет

        window.setResizable(false);
        window.getContentPane().add(content); // добавляю в рамку содержимое
        window.pack(); // задаю размер окна относительно содержимому
        window.setLocationRelativeTo(null); // размещаю окно по середине при запуске
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // при закрытии окна останавливаются потоки
        window.setVisible(true); // окно было видимым


        buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        bufferData = ((DataBufferInt) buffer.getRaster().getDataBuffer()).getData();
        bufferGraphics = buffer.getGraphics();
        ((Graphics2D) bufferGraphics).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // сглаживание
        clearColor = _clearColor;

        content.createBufferStrategy(numBuffers);
        bufferStrategy = content.getBufferStrategy();

        created = true;
    }

    public static void clear() {
        Arrays.fill(bufferData, clearColor);
    }

    public static void swapBuffers() {
        Graphics g = bufferStrategy.getDrawGraphics();
        g.drawImage(buffer, 0, 0, null);
        bufferStrategy.show();
    }

    public static Graphics2D getGraphics(){
        return (Graphics2D) bufferGraphics;
    }

    public static void destroy(){
        if (!created)
            return;

        window.dispose();

    }

    public static void setTitle(String title){
        window.setTitle(title);
    }


}

/*private static float  temp =0;
    private static float  temp1 =0;
    public static void render() {

        bufferGraphics.setColor(new Color(0x5D54FF));
        bufferGraphics.fillOval((int) (350 + (Math.sin(temp1) * 200)), 250, 100, 100);


        // включаю сглаживание
        ((Graphics2D) bufferGraphics).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        bufferGraphics.fillOval((int) (350 + (Math.sin(temp) * 200)), 150, 100, 100);

        // Выключить сглаживание
        ((Graphics2D) bufferGraphics).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

        temp += 0.02f;
        temp1 += 0.01f;
    }*/



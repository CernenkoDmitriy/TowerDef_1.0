package game.mobs;

import game.Field;
import game.Game;
import game.Values;

import java.awt.*;

public class Mob {

    private int mobHealth = 100;
    private int mobSize = 26;

    // определяю точку призыва

    private int startBlock = getStartBlock(Field.currentLvl);
    private float x = ((Game.WIDTH - Values.FIELD_WIDTH * Values.BLOCK_SIZE) / 2) + (startBlock % Values.FIELD_WIDTH * Values.BLOCK_SIZE);
    private float y = (startBlock + 2) / Values.FIELD_WIDTH * Values.BLOCK_SIZE;


    public int getStartBlock(int[] currentLvl) {
        for (int i = 0; i < currentLvl.length; i++) {
            if (currentLvl[i] == Values.MAP_PORTAL)
                return i;

        }
        return 0;
    }
    // определяю точку призыва - конец

    private int currentBlock = startBlock;
    private int prevBlock = startBlock;
    int tempCurrentBlock = currentBlock;
    private static final String KEY_move = "move";

    public void move() {
        int[] map = Field.currentLvl;

        System.out.println(x);
        System.out.println(y);
        System.out.println(currentBlock);




        if (map[currentBlock + 1] == 1 && currentBlock + 1 != prevBlock) {
            synchronized (KEY_move) {
                x += 2.0f;
                currentBlock = ((int) (y / Values.BLOCK_SIZE) * Values.FIELD_WIDTH + (int) ((x - 140)) / Values.BLOCK_SIZE);
            }
        } else if (currentBlock + Values.FIELD_WIDTH <= map.length &&
                map[currentBlock + Values.FIELD_WIDTH] == 1 && currentBlock + Values.FIELD_WIDTH != prevBlock) {
            synchronized (KEY_move) {
                y += 2.0f;
                currentBlock = ((int) (y / Values.BLOCK_SIZE) * Values.FIELD_WIDTH + (int) ((x - 140)) / Values.BLOCK_SIZE);
            }
        } else if (map[currentBlock - 1] == 1 && currentBlock - 1 != prevBlock) {
            synchronized (KEY_move) {
                x -= 2.0f;
                currentBlock = ((int) (y / Values.BLOCK_SIZE) * Values.FIELD_WIDTH + (int) ((x - 140)) / Values.BLOCK_SIZE);
            }
        } else if (map[currentBlock - Values.FIELD_WIDTH] == 1 && currentBlock - Values.FIELD_WIDTH != prevBlock) {
            synchronized (KEY_move) {
                y -= 2.0f;
                currentBlock = ((int) (y / Values.BLOCK_SIZE) * Values.FIELD_WIDTH + (int) ((x - 140)) / Values.BLOCK_SIZE);
            }
        }

        if (tempCurrentBlock != currentBlock) {
            prevBlock = tempCurrentBlock;
            tempCurrentBlock = currentBlock;
            System.out.println(" tempCurrentBlock " + tempCurrentBlock);
            System.out.println(" currentBlock " + currentBlock);
            System.out.println(" prevBlock " + prevBlock);
        }
    }

    public void drawMob(Graphics2D g) {
        g.setColor(Color.YELLOW);
        g.fillOval((int) x, (int) y, mobSize, mobSize);

    }


}

package game;

public class Values {

    public static final int MAP_EMPTY = 0;
    public static final int MAP_ROAD = 1;
    public static final int MAP_PORTAL = 2;
    public static final int MAP_CASTLE = 3;

    public static final int BLOCK_SIZE = 52;
    public static final int FIELD_WIDTH = 10;
    public static final int FIELD_HEIGHT = 8;

}

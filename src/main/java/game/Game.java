package game;


import display.Display;
import game.mobs.Mob;

import java.awt.*;
import java.util.ArrayList;

public class Game implements Runnable {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    public static final String TITLE = "TowerDef";
    public static final int CLEAR_COLOR = 0xff000000;
    public static final int NUM_BUFFERS = 3;

    public static final float UPDATE_RATE = 60.0f;
    public static final float UPDATE_INTERVAL = Time.SECOND / UPDATE_RATE;
    public static final long IDLE_TIME = 1; // мили секунда

    private boolean running;
    private Thread gameThread;
    private Graphics2D graphics;


    //my code

    private Field field = new Field();
    private Mob mob = new Mob();

    //end

    public Game() {
        running = false;
        Display.create(WIDTH, HEIGHT, TITLE, CLEAR_COLOR, NUM_BUFFERS);
        graphics = Display.getGraphics();
    }

    public synchronized void start() {
        if (running)
            return;

        running = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public synchronized void stop() {

        if (!running)
            return;

        running = false;

        try {
            gameThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        cleanup();
    }

    private void update() { // считать физику
        mob.move();

    }

    private void render() { //после посчета физики, рисуем картинку
        Display.clear();
        //temp

        field.drawField(graphics);
        mob.drawMob(graphics);

        //end
        Display.swapBuffers();

    }

    private void cleanup() { //закрытие ресурсов
        Display.destroy();
    }

    public void run() {

        int fps = 0;
        int upd = 0;
        int updLoop = 0;

        long count = 0;

        float delta = 0;

        long lastTime = Time.get();
        while (running) {
            long now = Time.get();
            long elapseTime = now - lastTime;
            lastTime = now;

            count += elapseTime;

            boolean render = false;
            delta += (elapseTime / UPDATE_INTERVAL);
            while (delta > 1) {
                update();
                upd++;
                delta--;
                if (render) {
                    updLoop++;
                } else {
                    render = true;
                }
            }
            if (render) {
                render();
                fps++;
            } else {
                try {
                    Thread.sleep(IDLE_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (count >= Time.SECOND) {
                Display.setTitle(TITLE + " || Fps: " + fps + " | Upd: " + upd + " | UpdLoop: " + updLoop);
                upd = 0;
                fps = 0;
                updLoop =0;
                count =0;
            }

        }
    }
}

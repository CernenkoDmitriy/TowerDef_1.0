package game;

public class Time {

    public static final long SECOND = 1000000000l; // 1 second

    public static long get(){
        return System.nanoTime();
    }


}

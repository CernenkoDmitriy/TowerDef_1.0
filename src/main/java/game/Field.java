package game;

import java.awt.*;

public class Field {




    private static int[] lvl_1 =
            {
                    0, 0, 0, 1, 1, 1, 1, 0, 0, 0,
                    2, 1, 0, 1, 0, 0, 1, 0, 0, 0,
                    0, 1, 0, 1, 0, 0, 1, 1, 1, 1,
                    0, 1, 0, 1, 0, 0, 0, 0, 0, 1,
                    0, 1, 0, 1, 0, 0, 0, 0, 0, 1,
                    0, 1, 1, 1, 0, 0, 1, 1, 1, 1,
                    0, 0, 0, 0, 0, 1, 1, 0, 0, 0,
                    0, 0, 3, 1, 1, 1, 0, 0, 0, 0,
            };

    public static int [] currentLvl = lvl_1;
    private int widthField = 10;
    private int heightField = 8;
    private int size = 52;

    public void drawField(Graphics2D g) {
        for (int i = 0; i < lvl_1.length; i++) {
            if (currentLvl[i] == 1) {
                g.setColor(Color.CYAN);
                g.fillRect((Game.WIDTH - widthField * size) / 2 + (i % widthField * size), (i / widthField * size), size, size);

            } else if (currentLvl[i] == 2) {
                g.setColor(Color.ORANGE);
                g.fillRect((Game.WIDTH - widthField * size) / 2 + (i % widthField * size),  (i / widthField * size), size, size);

            } else if (currentLvl[i] == 3) {
                g.setColor(Color.MAGENTA);
                g.fillRect((Game.WIDTH - widthField * size) / 2 + (i % widthField * size), (i / widthField * size), size, size);

            } else {
                g.setColor(Color.CYAN);
                g.drawRect((Game.WIDTH - widthField * size) / 2 + (i % widthField * size),  (i / widthField * size), size, size);
            }
        }
    }

}

